use mwapi_responses::prelude::*;

#[query(prop = "info|revisions", inprop = "url", rvprop = "ids")]
struct Response;

fn main() {
    dbg!(&Response::params());
}
