use mwbot::generators::Generator;

#[derive(Generator)]
#[params(generator = "foo")]
struct MissingParam {
    title: String,
}

fn main() {}
