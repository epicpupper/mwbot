// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright (C) 2023 Kunal Mehta <legoktm@debian.org>

/// Trait to represent a value that can be an API parameter value.
///
/// We need this as a separate thing because we can't set both a
/// blanket implementation on `Display` and `Vec<T: Display>`.
pub trait ParamValue {
    /// Serialize the value into a string for the API
    fn stringify(&self) -> String;
}

macro_rules! impl_value {
    ( $x:ty ) => {
        impl ParamValue for $x {
            fn stringify(&self) -> String {
                self.to_string()
            }
        }
    };
}

impl_value!(u32);
impl_value!(u64);
impl_value!(String);

impl<T: ParamValue> ParamValue for Vec<T> {
    fn stringify(&self) -> String {
        let x: Vec<_> = self.iter().map(|i| i.stringify()).collect();
        x.join("|")
    }
}
