mwbot-rs
========
[![pipeline status](https://gitlab.wikimedia.org/repos/mwbot-rs/mwbot/badges/main/pipeline.svg)](https://gitlab.wikimedia.org/repos/mwbot-rs/mwbot/-/commits/main)
[![coverage report](https://gitlab.com/mwbot-rs/mwbot/badges/main/coverage.svg)](https://mwbot-rs.gitlab.io/mwbot/coverage/)

mwbot-rs is a framework for writing bots and tools in Rust. The aim is to
provide the building blocks for people to write safe and concurrent
applications that interact with and enhance MediaWiki. The project is still
getting started, all contributions are welcome! 

See the [mwbot-rs wiki page](https://www.mediawiki.org/wiki/mwbot-rs) for more
details.

## Crates

| Crate                  | Version                                                                                                               | Documentation                                                                                          |
|------------------------|-----------------------------------------------------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------|
| mwbot                  | [![lib.rs](https://img.shields.io/crates/v/mwbot.svg)](https://lib.rs/crates/mwbot)                                   | [![docs.rs](https://docs.rs/mwbot/badge.svg)](https://docs.rs/mwbot)                                   |
| mwbot_derive           | [![lib.rs](https://img.shields.io/crates/v/mwbot_derive.svg)](https://lib.rs/crates/mwbot_derive)                     | [![docs.rs](https://docs.rs/mwbot_derive/badge.svg)](https://docs.rs/mwbot_derive)                     |
| mwapi                  | [![lib.rs](https://img.shields.io/crates/v/mwapi.svg)](https://lib.rs/crates/mwapi)                                   | [![docs.rs](https://docs.rs/mwapi/badge.svg)](https://docs.rs/mwapi)                                   |
| mwapi_responses        | [![lib.rs](https://img.shields.io/crates/v/mwapi_responses.svg)](https://lib.rs/crates/mwapi_responses)               | [![docs.rs](https://docs.rs/mwapi_responses/badge.svg)](https://docs.rs/mwapi_responses)               |
| mwapi_responses_derive | [![lib.rs](https://img.shields.io/crates/v/mwapi_responses_derive.svg)](https://lib.rs/crates/mwapi_responses_derive) | [![docs.rs](https://docs.rs/mwapi_responses_derive/badge.svg)](https://docs.rs/mwapi_responses_derive) |
| mwseaql                | [![lib.rs](https://img.shields.io/crates/v/mwseaql.svg)](https://lib.rs/crates/mwapi_responses_derive)                | [![docs.rs](https://docs.rs/mwseaql/badge.svg)](https://docs.rs/mwseaql)                               |
| mwtitle                | [![lib.rs](https://img.shields.io/crates/v/mwtitle.svg)](https://lib.rs/crates/mwtitle)                               | [![docs.rs](https://docs.rs/mwtitle/badge.svg)](https://docs.rs/mwtitle)                               |
| parsoid                | [![lib.rs](https://img.shields.io/crates/v/parsoid.svg)](https://lib.rs/crates/parsoid)                               | [![docs.rs](https://docs.rs/parsoid/badge.svg)](https://docs.rs/parsoid)                               |
