## 0.1.0 / 2023-04-02
* Increase MSRV to 1.63.

## 0.1.0-alpha.3 / 2023-02-20
* Only count the main references group towards the references size.
* Allow passing mutable `Wikicode` instances, with a warning in documentation.
* `parsoid_stylesheet()` returns CSS that highlights, as close as possible,
  elements which are counted towards prosesize.

## 0.1.0-alpha.2 / 2023-02-15
* Don't incorrectly count `<math>` tags.
* Use broader method of finding references.
* Internal improvements for optimization.

## 0.1.0-alpha.1 / 2023-02-14
* Initial release.
