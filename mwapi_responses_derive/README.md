mwapi_responses_derive
======================
[![crates.io](https://img.shields.io/crates/v/mwapi_responses_derive.svg)](https://lib.rs/crates/mwapi_responses_derive)
[![pipeline status](https://gitlab.wikimedia.org/repos/mwbot-rs/mwbot/badges/main/pipeline.svg)](https://gitlab.wikimedia.org/repos/mwbot-rs/mwbot/-/commits/main)
[![coverage report](https://gitlab.com/mwbot-rs/mwbot/badges/main/coverage.svg)](https://mwbot-rs.gitlab.io/mwbot/coverage/)


The `mwapi_responses_derive` crate provides the proc-macro implementation for
`mwapi_responses`. See that crate for documentation and more details.

## License
`mwapi_responses_derive` is (C) 2020-2021 Kunal Mehta <legoktm@debian.org>
under the GPL v3 or any later version.
